﻿using System;
using System.Security.Permissions;
using ATM;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AtmTest
{
    [TestClass]
    public class MyAccountTest
    {
        private IMyAccount myAccount = new MyAccount();

        [TestMethod]
        //[ExpectedException(Security)]
        public void MyAccount_Despoit_no_EnterPincode()
        {

        }

        [TestMethod]
        //[ExpectedException(Security)]
        public void MyAccount_Withdraw_no_EnterPincode()
        {

        }

        [TestMethod]
        public void MyAccount_EnterPincode_Deposite_positive_return_true()
        {
        }
        [TestMethod]
        public void MyAccount_EnterPincode_Deposite_zero_return_false()
        {

        }
        [TestMethod]
        public void MyAccount_EnterPincode_Deposite_negative_return_false()
        {

        }
        [TestMethod]
        public void MyAccount_EnterPincode_Deposite_100_Withdraw_1000_return_0()
        {

        }
        [TestMethod]
        public void MyAccount_EnterPincode_Deposite_100_Withdraw_50_return_50()
        {

        }
    }
}
