﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    public interface IMyStack
    {
        void Push(int item);
        int Pop();
        void Clear();
        int Peek();
        int Count
        {
            get;
        }
        bool IsEmpty
        {
            get;
        }
    }
}
