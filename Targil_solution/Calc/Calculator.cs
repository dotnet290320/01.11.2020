﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc
{
    public class Calculator
    {
        private bool m_is_on = false;
        public int X { get; set; }
        public int Y { get; set; }

        public int Add()
        {
            return X + Y;
        }
        public int Subtract()
        {
            return X - Y;
        }

        public static void ClearMathFromMemory()
        {
            // is math library loaded ...
            Console.WriteLine("Cleaning math library from memory");
        }
        public static void UploadMath()
        {
            Console.WriteLine("Loading math lib into memory");
        }

        public void TurnOn()
        {
            // is math library loaded ...
            Console.WriteLine("Turning on calculator");
            m_is_on = true;
        }
        public void TurnOff()
        {
            Console.WriteLine("Turning off calculator");
            m_is_on = false;
        }
    }
}
