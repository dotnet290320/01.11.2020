﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcHW
{
    static class TestData
    {
        internal const int Calculator_Add_Simple_X = 10;
        internal const int Calculator_Add_Simple_Y = 20;
        internal const int Calculator_Add_Simple_Result = 30;

        internal const int Calculator_Subtract_Simple_X = 45;
        internal const int Calculator_Subtract_Simple_Y = 22;
        internal const int Calculator_Subtract_Simple_Result = 23;
    }
}
