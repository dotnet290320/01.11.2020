﻿using System;
using Calc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalcHW
{
    [TestClass]
    public class CalculatorTest
    {

        #region Private Members

        private Calculator m_calculator;

        #endregion

        #region Test Initializers and Cleanups

        [AssemblyInitialize]
        public static void AssemblyInitializer(TestContext testContext)
        {
            Calculator.UploadMath();
        }

        [AssemblyCleanup]
        public static void AssemblyCleanuper()
        {
            Calculator.ClearMathFromMemory();
        }

        [TestInitialize]
        public void TestInitializer()
        {
            m_calculator = new Calculator();
            m_calculator.TurnOn();
        }

        [TestCleanup]
        public void TestCleanuper()
        {
            m_calculator.TurnOff();
        }

        #endregion

        [TestMethod]
        public void Calculator_Add()
        {
            // AAA
            // Arrange
            m_calculator.X = TestData.Calculator_Add_Simple_X;
            m_calculator.Y = TestData.Calculator_Add_Simple_Y;

            // Act
            int result = m_calculator.Add();

            // Assert
            Assert.AreEqual(result, TestData.Calculator_Add_Simple_Result);
        }

        [TestMethod]
        public void Calculator_Subtract()
        {
            // AAA
            // Arrange
            m_calculator.X = TestData.Calculator_Subtract_Simple_X;
            m_calculator.Y = TestData.Calculator_Subtract_Simple_Y;

            // Act
            int result = m_calculator.Subtract();

            // Assert
            Assert.AreEqual(result, TestData.Calculator_Subtract_Simple_Result);
        }
    }
}
