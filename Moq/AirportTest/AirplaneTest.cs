﻿using System;
using Airport;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AirportTest
{
    [TestClass]
    public class AirplaneTest
    {
        [TestMethod]
        public void Airplane_HighSpeed_over_800_true()
        {
            // moq
            // mock
            // fake it easy

            // cannot create it myself, or too much trouble
            // just want the engine to say "800"
            // when asked what is your speed ....
            // float GetSpeed();
            //IEngine engine = new Engine();
            Mock<IEngine> dummy_engine = new Mock<IEngine>();
            Airplane airplane = new Airplane(dummy_engine.Object);
            dummy_engine.Setup(f => f.GetSpeed()).Returns(800);

            Assert.IsTrue(airplane.HighSpeed());
        }

        [TestMethod]
        public void Airplane_HighSpeed_at_600_false()
        {
            Mock<IEngine> dummy_engine = new Mock<IEngine>();
            Airplane airplane = new Airplane(dummy_engine.Object);
            dummy_engine.Setup(f => f.GetSpeed()).Returns(600);

            Assert.IsFalse(airplane.HighSpeed());
        }
    }
}
