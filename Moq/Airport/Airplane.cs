﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airport
{
    public class Airplane : IAriplane
    {
        private IEngine m_engine;

        public bool HighSpeed()
        {
            if (m_engine.GetSpeed() > 750)
                return true;
              return false;
        }

        public Airplane(IEngine engine)
        {
            m_engine = engine;
        }
    }
}
