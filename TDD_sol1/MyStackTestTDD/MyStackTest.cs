﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyStack;

namespace MyStackTestTDD
{
    [TestClass]
    public class MyStackTest
    {
        private IMyStack m_myStack;

        [TestInitialize]
        public void TestInitializer()
        {
            m_myStack = new MyStack.MyStack();
        }

        [TestMethod]
        public void MyStack_Push_Pop_Empty()
        {
            m_myStack.Push(10);
            int ten = m_myStack.Pop();
            Assert.AreEqual(ten, 10);
            Assert.IsTrue(m_myStack.IsEmpty);
        }

        [TestMethod]
        public void MyStack_Push_Peek_Count_1()
        {
            m_myStack.Push(10);
            int ten = m_myStack.Peek();
            Assert.AreEqual(ten, 10);
            Assert.IsFalse(m_myStack.IsEmpty);
            Assert.AreEqual(m_myStack.Count, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(EmptyStackException))]
        public void MyStack_Pop_Empty_EmptyStackException()
        {
            m_myStack.Pop();
        }

        [TestMethod]
        [ExpectedException(typeof(EmptyStackException))]
        public void MyStack_Push_Clear_Pop_EmptyStackException()
        {
            m_myStack.Push(10);
            int ten = m_myStack.Pop();
            Assert.AreEqual(ten, 10);
            m_myStack.Clear();
            m_myStack.Pop();
        }

        [TestMethod]
        public void MyStack_Push_x3_Count_Equal_3()
        {
            m_myStack.Push(10);
            m_myStack.Push(10);
            m_myStack.Push(10);
            Assert.AreEqual(m_myStack.Count, 3);
        }

        [TestMethod]
        public void MyStack_Push_x2_Clear_IsEmpty_Is_True_Count_Is_0()
        {
            m_myStack.Push(10);
            m_myStack.Push(10);
            m_myStack.Clear();
            Assert.IsTrue(m_myStack.IsEmpty);
            Assert.AreEqual(m_myStack.Count, 0);
        }
    }

}
