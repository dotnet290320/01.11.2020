﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    public class MyStack : IMyStack
    {
        private List<int> m_numbers = new List<int>();
        public int Count
        {
            get
            {
                return m_numbers.Count;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return m_numbers.Count == 0;
            }
        }

        public void Clear()
        {
            m_numbers.Clear();
        }

        public int Peek()
        {
            if (IsEmpty)
                throw new EmptyStackException("Cannot peek empty stack");
            return m_numbers[0];
        }

        public int Pop()
        {
            if (IsEmpty)
                throw new EmptyStackException("Cannot pop empty stack");
            int result = m_numbers[0];
            m_numbers.RemoveAt(0);
            return result;
        }

        public void Push(int item)
        {
            m_numbers.Add(item);
        }
    }
}
